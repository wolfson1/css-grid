import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { HostListener } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';

@Component({
  selector: 'app-cell',
  templateUrl: './cell.component.html',
  styleUrls: ['./cell.component.css']
})
export class CellComponent implements OnInit {
  @Input() public readonly data: { title: string; details: {key: string; value: string}[] };
  @HostBinding('class.large') public large: true | null = null;
  @HostBinding('class.specific') public specific: true | null = null;

  public ngOnInit(): void {
    this.specific = this.data.details.length > 7 ? true : null;
  }

  @HostListener('click')
  private onClick(): void {
    this.large = this.large ? null : true;
  }
}
