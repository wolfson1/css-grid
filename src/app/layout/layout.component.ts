import { Component, HostBinding } from '@angular/core';
import { Input } from '@angular/core';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css'],
  animations: []
})
export class LayoutComponent {
  @Input() public readonly data: {title: string}[];
}
